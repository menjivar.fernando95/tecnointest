package com.fernnx2.eshop;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.TestPropertySource;

@SpringBootTest
@TestPropertySource("classpath:applicationtest.properties")
class EshopApplicationTests {

	@Test
	void contextLoads() {
        Assert.assertTrue(true);
	}

}
