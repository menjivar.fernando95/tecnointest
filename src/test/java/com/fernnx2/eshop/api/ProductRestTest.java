package com.fernnx2.eshop.api;

import com.c4_soft.springaddons.security.oauth2.test.annotations.WithMockAuthentication;
import com.c4_soft.springaddons.security.oauth2.test.annotations.keycloak.WithMockKeycloakAuth;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fernnx2.eshop.dto.ProductDTO;
import com.fernnx2.eshop.models.Product;
import com.fernnx2.eshop.services.ProductService;
import org.json.JSONException;
import org.json.JSONObject;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.keycloak.adapters.springboot.KeycloakSpringBootConfigResolver;
import org.keycloak.adapters.springsecurity.KeycloakSecurityComponents;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;

import org.springframework.boot.test.mock.mockito.MockBean;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(ProductRest.class)
@ContextConfiguration
@ComponentScan(basePackageClasses = { KeycloakSecurityComponents.class, KeycloakSpringBootConfigResolver.class })
public class ProductRestTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ProductService ps;

    static Product x;
    static Product y;
    static ProductDTO z;
    static Product zx;
    static List<Product> productList = new ArrayList<Product>();

    static String access_token;

    HttpHeaders headers = new HttpHeaders();

    @Autowired
    private WebApplicationContext context;

    @BeforeClass
    public static void setUp() throws URISyntaxException, JSONException {
         x = new Product(1L,"FM1","Huawei");
         y = new Product(2L,"FM2","Iphone");
         z = new ProductDTO(3L,"FM3","Iphone");
         zx = new Product(3L,"FM3","Iphone");
         productList.add(x);
         productList.add(y);

       String createPersonUrl = "https://git.tecnoin.space/auth/realms/test/protocol/openid-connect/token";


        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.valueOf(MediaType.APPLICATION_FORM_URLENCODED_VALUE));
       // headers.add("Content-Type", "application/x-www-form-urlencoded");


        MultiValueMap<String, String> bodyMap = new LinkedMultiValueMap<>();
        bodyMap.add("client_id", "test_client");
        bodyMap.add("username", "admin_test");
        bodyMap.add("password", "Test2022");
        bodyMap.add("grant_type", "password");


        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(bodyMap, headers);
        String result = restTemplate.postForObject(createPersonUrl, requestEntity, String.class);
        JSONObject jsonObject = new JSONObject(result);
        access_token = (String) jsonObject.get("access_token");

    }


    @Test
    @WithMockAuthentication()
    public void getAllProduct_thenAccept() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        String p1 = objectMapper.writeValueAsString(x);
        String p2 = objectMapper.writeValueAsString(y);
        given(this.ps.findAllProduct()).willReturn(productList);
        headers.add("Authorization","Bearer " + access_token);
        this.mvc.perform(get("/product").headers(headers).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json("[" + p1 + "," + p2 + "]"));

    }

    @Test
    public void getProduct_thenAccept() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        String p1 = objectMapper.writeValueAsString(x);
        given(this.ps.findProductById(x.getId())).willReturn(productList.stream().findFirst());
        headers.add("Authorization","Bearer " + access_token);
        this.mvc.perform(get("/product/1").headers(headers).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(p1));
    }

    @Test
    public void saveProduct_thenAccept() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        String p1 = objectMapper.writeValueAsString(z);
        given(this.ps.saveOrUpdate(zx)).willReturn(zx);
        headers.add("Authorization","Bearer " + access_token);
        this.mvc.perform(post("/product").headers(headers)
                .content(p1)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(p1));
    }

    @Test
    public void deleteProduct_thenAccept() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        String p1 = objectMapper.writeValueAsString(x);
        given(this.ps.deleteProduct(x.getId())).willReturn(true);
        headers.add("Authorization","Bearer " + access_token);
        this.mvc.perform(delete("/product/1").headers(headers)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
