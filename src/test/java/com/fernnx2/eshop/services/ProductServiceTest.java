package com.fernnx2.eshop.services;

import com.fernnx2.eshop.models.Product;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource("classpath:applicationtest.properties")
public class ProductServiceTest {

    @Autowired
    ProductService ps;

    static Product p = new Product();


    @BeforeClass
    public static void beforeClassTest(){
        p.setCodigo("FM1");
        p.setNombre("Huawei");


    }

    @Test
    public void findAll_thenAccept(){
        Product y = new Product();
        y.setCodigo("FM1");
        y.setNombre("Huawei");
        ps.saveOrUpdate(y);
        Assert.assertEquals(1, ps.findAllProduct().stream().count());
    }

    @Test
    public void saveOrUpdate_thenAccept(){
        Product x = ps.saveOrUpdate(p);
        Assert.assertEquals(x, p);
    }

    @Test
    public void findProductById_thenAccept(){
        Assert.assertEquals(this.ps.findProductById(p.getId()).get(), p);
    }

    @Test
    public void deleteProduct_thenAccept(){
        Assert.assertTrue(this.ps.deleteProduct(1L));
    }



}
