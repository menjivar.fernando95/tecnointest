package com.fernnx2.eshop.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

//@EnableWebSecurity
public class SecurityConfig /*extends WebSecurityConfigurerAdapter*/ {
/*
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .csrf()
                .disable().authorizeRequests()
                .antMatchers("product").
                hasAnyRole("BO_ADMIN","BO_USUARIO")
                .and().formLogin();
        http.
                csrf()
                .disable()
                .authorizeRequests()
                .antMatchers("product/")
                .hasAnyRole("BO_USUARIO")
                .and().formLogin();
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("product")
                .hasAnyRole("BO_ADMIN", "BO_USUARIO")
                .anyRequest().permitAll()
                .and()
                .httpBasic()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }


    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception{
        PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        auth.inMemoryAuthentication().withUser("admin").password(encoder.encode("1234")).roles("BO_ADMIN");
        auth.inMemoryAuthentication().withUser("tecnoin").password(encoder.encode("1234")).roles("BO_USUARIO");
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

 */
}
