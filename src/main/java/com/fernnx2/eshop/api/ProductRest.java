package com.fernnx2.eshop.api;

import com.fernnx2.eshop.dto.ProductDTO;
import com.fernnx2.eshop.models.Product;
import com.fernnx2.eshop.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "product")
public class ProductRest {

    @Autowired
    ProductService ps;

    @RolesAllowed({"BO_ADMIN","BO_USUARIO"})
    @GetMapping
    public ResponseEntity<List<Product>> getAllProducts(){
        try{
            return new ResponseEntity<>(this.ps.findAllProduct(), HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RolesAllowed({"BO_ADMIN"})
    @GetMapping("/{id}")
    public ResponseEntity<Product> getProduct(@PathVariable("id") Long id){
        try{
            Optional<Product> product = this.ps.findProductById(id);
            if(product.isPresent()){
                return new ResponseEntity<>(product.get(), HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>( HttpStatus.NOT_FOUND);
            }

        }catch(Exception e){
            return new ResponseEntity<>( HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RolesAllowed({"BO_ADMIN"})
    @PostMapping
    public ResponseEntity<Product> save(@RequestBody ProductDTO product){
        try{
            var p = new Product();
            p.setId(product.getId());
            p.setCodigo(product.getCodigo());
            p.setNombre(product.getNombre());
            return new ResponseEntity<>( this.ps.saveOrUpdate(p), HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RolesAllowed({"BO_ADMIN"})
    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteProduct(@PathVariable("id") Long id){
        try{
            this.ps.deleteProduct(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch(Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
