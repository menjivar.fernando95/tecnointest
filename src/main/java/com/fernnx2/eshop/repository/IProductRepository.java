package com.fernnx2.eshop.repository;

import com.fernnx2.eshop.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;
@Repository
public interface IProductRepository extends JpaRepository<Product, Long> {
}
